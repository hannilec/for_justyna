#In working test framework please create a test that will:
#Create a new Lead
#Open the Lead detail page.
#Add a new Note for that Lead.
#Check if the Note shows up on the list.
require 'spec_helper'

def go_back_to_leads_page
  on(NavigationPage).leads_element.when_visible.click
  on(LeadsPage).leads_list_element.when_visible
end 

def get_created_lead_from_list lname
  go_back_to_leads_page
  #on(LeadsPage).get_lead lname
end


shared_examples "Create lead and check it" do |lname, content|
  it "Open add lead page" do
    on(LeadsPage).add_new_lead_element.when_visible.click
  end

  it "Enter lead last name" do
    on(LeadNewPage).last_name_element.when_visible(10).value = lname
  end

  it "Save lead" do
    on(LeadNewPage).save_lead
    on(LeadNewPage).paragraph_element(:class => "info-label").when_visible
  end

  it "Check if lead is visible on index" do
    #lead = get_created_lead_from_list lname
    #expect(lead[0]).not_to be_nil
    go_back_to_leads_page
    expect(on(LeadsPage).visible? lname).to be true
  end

  it "Edit lead" do
    on(LeadsPage).edit_lead lname
  end

  it "Add note" do
    on(LeadPage).add_note content
  end

  it "Check if note is visible" do
    expect(on(LeadPage).note_visible? content).to be true
  end
end

describe "Leads" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end

  describe "Add a new lead and note to him" do
    include_examples "Create lead and check it", "test_add_new_lead", "add note to the lead"
  end
end
