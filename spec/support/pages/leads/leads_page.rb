class LeadsPage < AbstractPage
  include PageObject
  include RelatedToPicker

  page_url "https://app.futuresimple.com/leads"

  link(:add_new_lead, :id => 'leads-new')
  div(:leads_list, :class => "objects")
  list_item(:empty_info, :class => "item empty")

  def get_lead lname
    lead = []
    leads_list_element.when_visible.list_item_elements.each do |item|
      if item.h3_element.exists? and item.h3_element.text == lname
	  lead << item          
      end
    end
    lead
  end

  def visible? lname
    return self.link_element(:class => "lead-name", :text => lname).when_visible(10).visible?
  end

  def edit_lead lname
    self.link_element(:class => "lead-name", :text => lname).when_visible(5).click
  end
end
