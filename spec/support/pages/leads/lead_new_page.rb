class LeadNewPage < AbstractPage
  include PageObject

  page_url "https://app.futuresimple.com/leads/new"

  button(:save_lead, :css => ".save")
  text_field(:last_name, :name => "last_name")
  
end
