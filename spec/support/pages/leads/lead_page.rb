class LeadPage < AbstractPage
  include PageObject
  include RelatedToPicker

  #page_url "https://app.futuresimple.com/leads/..."

  textarea(:note_text)
  button(:save_button, :text => "Save")

  def add_note content
     note_text_element.when_visible(5).value = content
     save_button_element.when_visible(5).click
  end

  def note_visible? content
     return self.div_element(:text => content).when_visible(5).visible?
  end
end
